#!/bin/bash

cargo build --release;

if [[ -f "/usr/local/bin/pgc" ]]; then
  rm /usr/local/bin/pgc;
fi

mv target/release/pgc /usr/local/bin;

# Setup completions.

if [[ "$OSTYPE" == "darwin"* ]]; then
  completions_dir="$ZSH/completions";
  mkdir -p "$completions_dir";
  completions_file="$completions_dir/_pgc";
  pgc completions zsh > "$completions_file";
fi
