use anyhow::Result;
use clap_generate::{generate, generators::*};
use pgc_db::{ConnCfg, ConnCfgs};
use std::io;
use std::{fs, path, process};

mod arg_parser;

const XDG_PREFIX: &str = "pgc";
const XDG_CONN_CFG_FILE: &str = "conn_cfg.toml";

fn display_conn_cfg(conn_cfg: &ConnCfg) -> String {
    let name_str = format!("{}:\n", conn_cfg.name);

    let host_str = conn_cfg
        .host
        .as_ref()
        .map(|host| format!("- host: {}\n", host))
        .unwrap_or_else(String::new);
    let db_str = conn_cfg
        .db
        .as_ref()
        .map(|db| format!("- db: {}\n", db))
        .unwrap_or_else(String::new);
    let user_str = conn_cfg
        .user
        .as_ref()
        .map(|user| format!("- user: {}\n", user))
        .unwrap_or_else(String::new);
    let pwd_str = conn_cfg
        .pwd
        .as_ref()
        .map(|pwd| {
            format!(
                "- pwd: {}\n",
                (0..pwd.len()).map(|_| '*').collect::<String>()
            )
        })
        .unwrap_or_else(String::new);

    let mut result = String::new();

    result.push_str(&name_str);
    result.push_str(&host_str);
    result.push_str(&db_str);
    result.push_str(&user_str);
    result.push_str(&pwd_str);

    result
}

fn handle_add_subcmd(conn_cfg_path: path::PathBuf, stub: &clap::ArgMatches) -> Result<()> {
    let name: String = stub.value_of("NAME").unwrap().to_string();
    let host: Option<String> = stub.value_of("host").map(str::to_string);
    let pwd: Option<String> = stub.value_of("pwd").map(str::to_string);
    let db: Option<String> = stub.value_of("db").map(str::to_string);
    let user: Option<String> = stub.value_of("user").map(str::to_string);

    let mut cfgs = ConnCfgs::read_path(&conn_cfg_path)?;
    let cfg: ConnCfg = ConnCfg::new(name, host, db, user, pwd);
    cfgs.add(cfg);
    cfgs.write_path(&conn_cfg_path)?;

    Ok(())
}

fn handle_list_subcmd(conn_cfg_path: path::PathBuf) -> Result<()> {
    let cfgs = ConnCfgs::read_path(&conn_cfg_path)?;

    if let Some(conn_cfg) = cfgs.conn_cfg {
        for cfg in conn_cfg {
            println!("{}", display_conn_cfg(&cfg));
        }
    }

    Ok(())
}

fn handle_rm_subcmd(conn_cfg_path: path::PathBuf, stub: &clap::ArgMatches) -> Result<()> {
    let name: String = stub.value_of("NAME").unwrap().to_string();

    let mut cfgs = ConnCfgs::read_path(&conn_cfg_path)?;
    cfgs.rm(&name)?;
    cfgs.write_path(&conn_cfg_path)?;

    println!("\"{}\" connection config removed.", name);

    Ok(())
}

fn handle_run_subcmd(conn_cfg_path: path::PathBuf, stub: &clap::ArgMatches) -> Result<()> {
    let name: String = stub.value_of("NAME").unwrap().to_string();
    let command: Option<String> = stub.value_of("COMMAND").map(str::to_string);

    let mut cfgs = ConnCfgs::read_path(&conn_cfg_path)?;
    let cfg = cfgs.get(&name)?;

    let mut psql_cmd: process::Command = cfg.into();

    if let Some(command) = command {
        psql_cmd.arg("-c");
        psql_cmd.arg(&command);

        // Disable pager to ensure non-interactive.
        psql_cmd.env("PSQL_PAGER", "");
        psql_cmd.env("PAGER", "");
    }

    psql_cmd.spawn().unwrap().wait().unwrap();

    Ok(())
}

fn handle_print_subcmd(conn_cfg_path: path::PathBuf, stub: &clap::ArgMatches) -> Result<()> {
    let name: String = stub.value_of("NAME").unwrap().to_string();

    let mut cfgs = ConnCfgs::read_path(&conn_cfg_path)?;
    let cfg = cfgs.get(&name)?;

    println!("{}", cfg);

    Ok(())
}

fn handle_completions_subcmd(app: &mut clap::App, stub: &clap::ArgMatches) -> Result<()> {
    let name: String = stub.value_of("SHELL").unwrap().to_string();

    match name.as_str() {
        "zsh" => generate::<Zsh, _>(app, "pgc", &mut io::stdout()),
        "elvish" => generate::<Elvish, _>(app, "pgc", &mut io::stdout()),
        "fish" => generate::<Fish, _>(app, "pgc", &mut io::stdout()),
        "powershell" => generate::<PowerShell, _>(app, "pgc", &mut io::stdout()),
        "bash" => generate::<Bash, _>(app, "pgc", &mut io::stdout()),
        _ => (), // Should not happen.
    }

    Ok(())
}

fn main() -> Result<()> {
    let xdg_dirs = xdg::BaseDirectories::with_prefix(XDG_PREFIX).unwrap();
    let conn_cfg_path = match xdg_dirs.find_config_file(XDG_CONN_CFG_FILE) {
        Some(p) => p,
        None => {
            let file_name = xdg_dirs.place_config_file(XDG_CONN_CFG_FILE)?;
            fs::File::create(&file_name)?;
            file_name
        }
    };

    let app = arg_parser::parse_pargs();
    let mut gen_app = app.clone();
    let matches = app.get_matches();
    match matches.subcommand().unwrap() {
        ("add", stub) => handle_add_subcmd(conn_cfg_path, stub)?,
        ("ls", _) => handle_list_subcmd(conn_cfg_path)?,
        ("rm", stub) => handle_rm_subcmd(conn_cfg_path, stub)?,
        ("run", stub) => handle_run_subcmd(conn_cfg_path, stub)?,
        ("print", stub) => handle_print_subcmd(conn_cfg_path, stub)?,
        ("completions", stub) => handle_completions_subcmd(&mut gen_app, stub)?,
        _ => (), // Should never happen.
    };

    Ok(())
}
