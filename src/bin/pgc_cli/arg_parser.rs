use clap::clap_app;

pub fn parse_pargs<'a>() -> clap::App<'a> {
    let app = clap_app!(app =>
        (version: "0.1")
        (name: "pgc")
        (about: "A utility to manage postgres connections")
        (author: "Saimir S. <hola@saimirsulaj.com>")
        (@setting SubcommandRequiredElseHelp)
        (@subcommand add =>
            (name: "add")
            (about: "Adds a connection config")
            (@arg host: -h --host +takes_value "The host URL")
            (@arg pwd: -p --pwd +takes_value "The password")
            (@arg db: -d --db +takes_value "The database")
            (@arg user: -u --user +takes_value "The user")
            (@arg NAME: +takes_value +required "The name of the connection config")
        )
        (@subcommand ls =>
            (name: "ls")
            (about: "List all available connections")
        )
        (@subcommand rm =>
            (name: "rm")
            (about: "Removed a connection config")
            (@arg NAME: +takes_value +required "The name of the connection config")
        )
        (@subcommand run =>
            (name: "run")
            (about: "Runs a connection config")
            (@arg NAME: +takes_value +required "The name of the connection config")
            (@arg COMMAND: -c --command +takes_value "The query to run in psql")
        )
        (@subcommand print =>
            (name: "print")
            (about: "Prints the command to open a connection")
            (@arg NAME: +takes_value +required "The name of the connection config")
        )
        (@subcommand completions =>
            (name: "completions")
            (about: "Generates completions")
            (@arg SHELL: +takes_value +required possible_value[bash zsh elvish fish powershell] "The name of the shell")
        )
    );

    app
}
