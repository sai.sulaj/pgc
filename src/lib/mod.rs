use anyhow::Result;
use serde_derive::{Deserialize, Serialize};
use std::io::prelude::*;
use std::{fmt, fs, path, process};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum PgcError {
    #[error("Unable to serialize config: {0}")]
    SerConnCfg(#[source] toml::ser::Error),
    #[error("Unable to deserialize config: {0}")]
    DeConnCfg(#[source] toml::de::Error),
    #[error("Connection config \"{0}\" not found")]
    ConnCfgNotFound(String),
}

#[derive(Deserialize, Serialize)]
pub struct ConnCfg {
    pub name: String,
    pub host: Option<String>,
    pub db: Option<String>,
    pub user: Option<String>,
    pub pwd: Option<String>,
}
impl ConnCfg {
    pub fn new(
        name: String,
        host: Option<String>,
        db: Option<String>,
        user: Option<String>,
        pwd: Option<String>,
    ) -> Self {
        Self {
            name,
            host,
            db,
            user,
            pwd,
        }
    }

    pub fn build_cmd(&self, prepend_pwd: bool) -> String {
        let mut psql_cmd = String::new();
        if let Some(pwd) = self.pwd.as_ref() {
            if prepend_pwd {
                psql_cmd.push_str(&format!("PGPASSWORD=\"{}\" ", pwd));
            }
        }
        psql_cmd.push_str("psql ");
        if let Some(host) = self.host.as_ref() {
            psql_cmd.push_str(&format!("-h \"{}\" ", host));
        }
        if let Some(db) = self.db.as_ref() {
            psql_cmd.push_str(&format!("-d \"{}\" ", db));
        }
        if let Some(user) = self.user.as_ref() {
            psql_cmd.push_str(&format!("-U \"{}\" ", user));
        }

        psql_cmd
    }

    pub fn to_clipboard(&self) {
        let psql_cmd = self.build_cmd(true);

        let pbcopy_cmd = process::Command::new("pbcopy")
            .stdin(process::Stdio::piped())
            .stdout(process::Stdio::piped())
            .spawn()
            .unwrap(); // Why would this fail.

        pbcopy_cmd
            .stdin
            .unwrap()
            .write_all(psql_cmd.as_bytes())
            .unwrap();
    }
}
impl fmt::Display for ConnCfg {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(pwd) = self.pwd.as_ref() {
            write!(f, "PGPASSWORD=\"{}\" ", pwd)?;
        }
        write!(f, "psql ")?;
        if let Some(host) = self.host.as_ref() {
            write!(f, "-h \"{}\" ", host)?;
        }
        if let Some(db) = self.db.as_ref() {
            write!(f, "-d \"{}\" ", db)?;
        }
        if let Some(user) = self.user.as_ref() {
            write!(f, "-U \"{}\" ", user)?;
        }

        Ok(())
    }
}
impl From<&ConnCfg> for process::Command {
    fn from(cfg: &ConnCfg) -> Self {
        let mut psql_cmd = process::Command::new("psql");

        if let Some(host) = cfg.host.as_ref() {
            psql_cmd.arg("-h");
            psql_cmd.arg(host);
        }
        if let Some(db) = cfg.db.as_ref() {
            psql_cmd.arg("-d");
            psql_cmd.arg(db);
        }
        if let Some(user) = cfg.user.as_ref() {
            psql_cmd.arg("-U");
            psql_cmd.arg(user);
        }
        if let Some(pwd) = cfg.pwd.as_ref() {
            psql_cmd.env("PGPASSWORD", pwd);
        }

        psql_cmd
    }
}

#[derive(Deserialize, Serialize)]
pub struct ConnCfgs {
    pub conn_cfg: Option<Vec<ConnCfg>>,
}
impl ConnCfgs {
    pub fn ser(&self) -> Result<String> {
        let encoded: String = toml::to_string(self).map_err(PgcError::SerConnCfg)?;
        Ok(encoded)
    }

    pub fn de(value: &str) -> Result<Self> {
        let decoded: Self = toml::from_str(value).map_err(PgcError::DeConnCfg)?;
        Ok(decoded)
    }

    pub fn read_path(path: &path::Path) -> Result<Self> {
        let mut file = fs::File::open(path)?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;

        let cfg = Self::de(&contents)?;
        Ok(cfg)
    }

    pub fn write_path(&self, path: &path::Path) -> Result<()> {
        let content = self.ser()?;

        let mut file = fs::File::create(path)?;
        file.write_all(content.as_bytes())?;

        Ok(())
    }

    pub fn add(&mut self, new_conn_cfg: ConnCfg) {
        if self.conn_cfg.is_none() {
            self.conn_cfg = Some(Vec::new());
        }

        self.conn_cfg.as_mut().unwrap().push(new_conn_cfg);
    }

    pub fn rm(&mut self, name: &str) -> Result<()> {
        if self.conn_cfg.is_none() {
            self.conn_cfg = Some(Vec::new());
        }

        let cfgs = self.conn_cfg.as_mut().unwrap();

        let index = cfgs.iter().position(|cfg| cfg.name == name);
        match index {
            Some(index) => {
                cfgs.remove(index);
                Ok(())
            }
            None => Err(PgcError::ConnCfgNotFound(name.to_string()).into()),
        }
    }

    pub fn get(&mut self, name: &str) -> Result<&ConnCfg> {
        if self.conn_cfg.is_none() {
            self.conn_cfg = Some(Vec::new());
        }

        let cfgs = self.conn_cfg.as_mut().unwrap();

        let index = cfgs.iter().position(|cfg| cfg.name == name);
        match index {
            Some(index) => Ok(cfgs.get(index).unwrap()),
            None => Err(PgcError::ConnCfgNotFound(name.to_string()).into()),
        }
    }
}
